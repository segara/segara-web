// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: "Segara",
  siteUrl: "https://segara.io",
  siteDescription:
    "Segara aporta la solución definitiva para la medición, reducción y compensación de la huella de carbono de empresas y organizaciones.",
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "src/data/articles/*.md",
        typeName: "Article",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "src/data/authors/*.md",
        typeName: "Author",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "src/data/posts/*.md",
        typeName: "Post",
        refs: {
          author: "Author",
        },
      },
    },
    {
      use: "gridsome-plugin-matomo",
      options: {
        host: "https://nosturi.es/matomo/",
        siteId: 3,
      },
    },
    {
      use: "gridsome-plugin-i18n",
      options: {
        locales: ["es", "en"],
        fallbackLocale: "es",
        defaultLocale: "es",
        enablePathRewrite: true,
        rewriteDefaultLanguage: true,
      },
    },
    {
      use: "@gridsome/plugin-sitemap",
    },
  ],
  transformers: {
    remark: {
      autolinkHeadings: false,
      plugins: ["remark-attr"],
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set("@images", "@/assets/images");
  },
  templates: {
    Article: [
      {
        path: "/article/:slug/",
        component: "./src/templates/Article.vue",
      },
    ],
    Post: [
      {
        path: "/post/:year/:month/:slug/",
        component: "./src/templates/Post.vue",
      },
    ],
    Author: [
      {
        path: "/author/:slug/",
        component: "./src/templates/Author.vue",
      },
    ],
  },
};
