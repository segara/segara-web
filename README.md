# Segara Website

Repository for the website <https://segara.io>.

## Technical details

- Static site made with [Gridsome](https://gridsome.org/) and [Buefy](https://buefy.org).
- Development dependencies can be found on `package.json`.
- Data is imported from the Markdown files in `/src/data` directory.
- Build is automatically run every time a new commit is pushed to branch `master`.

## Accessibility

We believe in **universal design** and we try to make our products accessible to everyone. Although we use [Axe](https://github.com/dequelabs/axe-core) on a daily basis to audit and improve our website accesibility, automated tests are not enough. Please feel free to open a Merge Request if you find an accessibility issue, and we will look into it ASAP!

## Licenses

The code is released under [MIT License](https://mit-license.org/).

The content is released under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Segara &copy;, Segara logo and other brands and logos published in this website are registered trademarks of Segara Environmental Consulting, Ltd. All Segara trademarks cannot be used without owner's formal permission. For information and more details about the proper use of our trademarks, please write to <info@segara.io>.
