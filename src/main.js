// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from "~/layouts/Default.vue";

// import FontAwesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
  faBuilding,
  faCalculator,
  faCertificate,
  faCheckSquare,
  faEnvelope,
  faIndustry,
  faInfinity,
  faMapMarkerAlt,
  faQuestion,
  faSolarPanel,
  faUser,
  faMobileAlt,
  faCoffee,
  faGlobeEurope,
} from "@fortawesome/free-solid-svg-icons";
import {
  faOsi,
  faTwitter,
  faLinkedin,
  faGitlab,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

// import Buefy
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import "~/assets/style/index.scss";

// import Swiper
import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/css/swiper.css";

export default function(Vue, { head, appOptions }) {
  head.script.push({
    src: "https://smtpjs.com/v3/smtp.js",
    body: true,
  });
  head.script.push({
    src: "https://unpkg.com/sweetalert/dist/sweetalert.min.js",
    body: true,
  });
  head.script.push({
    src: "https://unpkg.com/swiper/swiper-bundle.min.js",
    body: true,
  });
  head.link.push({
    rel: "stylesheet",
    href: "https://unpkg.com/swiper/swiper-bundle.min.css",
  });
  library.add(
    faCalculator,
    faQuestion,
    faCertificate,
    faIndustry,
    faInfinity,
    faCheckSquare,
    faSolarPanel,
    faMapMarkerAlt,
    faEnvelope,
    faUser,
    faBuilding,
    faMobileAlt,
    faOsi,
    faTwitter,
    faLinkedin,
    faGitlab,
    faInstagram,
    faCoffee,
    faGlobeEurope
  );

  appOptions.i18n.setLocaleMessage("es", require("./locales/es.json"));
  appOptions.i18n.setLocaleMessage("en", require("./locales/en.json"));

  Vue.component("Layout", DefaultLayout);
  Vue.component("vue-fontawesome", FontAwesomeIcon);
  Vue.use(Buefy, {
    defaultIconComponent: "vue-fontawesome",
    defaultIconPack: "fas",
  });
  Vue.use(VueAwesomeSwiper);
}
