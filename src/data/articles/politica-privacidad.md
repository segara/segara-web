---
title: Política de privacidad
slug: politica-privacidad
---

# Responsable del tratamiento de datos

El responsable del tratamiento de tus datos personales es:

**Segara Environmental Consulting, SL**  
NIF B42890939  
Paseo de la Castellana, 194  
28046 Madrid

Puedes ponerte en contacto con el responsable del tratamiento dirigiendo un correo electrónico a la dirección <gdpr@segara.io>.

# Datos recogidos y finalidad del tratamiento

Para navegar por nuestro sitio web no es necesario que facilites ningún dato personal.

Nuestra herramienta de analítica web está configurada para ocultar los dos últimos bytes de las direcciones IP (por ejemplo, 192.168.xxx.xxx), anonimizar los datos de localización del usuario y no utilizar cookies que guarden datos personales. Los datos de navegación almacenados en la base de datos no permiten identificar al usuario y, por lo tanto, no pueden considerarse datos de carácter personal. Puedes leer más sobre cómo protegemos tu privacidad en el sitio web de [Matomo](https://matomo.org/blog/2018/04/how-to-not-process-any-personal-data-with-matomo-and-what-it-means-for-you/).

Al contactar con nosotros a través de los formularios, el correo electrónico o el teléfono disponibles en la web, solicitamos datos personales, entre los cuales pueden figurar tu nombre y apellidos, tu dirección de correo electrónico, tu teléfono, el nombre de la empresa o institución en nombre de la cual escribes o tu CV. Utilizamos estos datos únicamente para responder tus consultas, solicitudes de soporte, reclamaciones o candidatura laboral, lo que puede incluir la elaboración y la presentación de una propuesta comercial personalizada en el caso de consultas relativas a nuestros servicios.

Estos datos no serán incorporados a una lista de distribución, pero sí podrán pasar a formar parte de nuestra base de datos de clientes si terminas formalizando un contrato con nosotros, con la finalidad de poder llevar a cabo el objetivo contractual entre las partes.

No recogemos, en ningún caso, datos que, por su naturaleza, sean particularmente sensibles en relación con los derechos y las libertades fundamentales, como tu filiación política o religiosa.

No cedemos tus datos a terceros ni los transferimos fuera de la UE, salvo obligaciones previstas en la legislación vigente (requerimientos judiciales, fiscales, entre otros).

# Plazo de conservación

Los datos proporcionados se conservarán mientras se mantenga la relación comercial o durante los años necesarios para cumplir con las obligaciones legales.

# Medidas de seguridad

El tratamiento de los datos personales facilitados se lleva a cabo adoptando las medidas técnicas y organizativas necesarias para evitar la pérdida, uso indebido, alteración y acceso no autorizado a los mismos, habida cuenta del estado de la tecnología, la naturaleza de los datos y el análisis de riesgos efectuado.

En concreto, nuestro sitio web está protegido mediante un certificado SSL que cifra las comunicaciones entre tu navegador web y nuestros servidores. Puedes leer más sobre nuestro certificado en el sitio web de [Let’s Encrypt](https://letsencrypt.org/about/).

Además, para garantizar al máximo la protección de tus datos personales, trabajamos con un proveedor de correo electrónico seguro, con sede en la Unión Europea y que no analiza el contenido de los correos electrónicos. Puedes leer más sobre la seguridad de nuestro correo en el sitio web de [Mailbox.org](https://mailbox.org/en/security).

# Ejercicio de derechos

Puedes ejercer tus derechos de acceso, rectificación, oposición, supresión, limitación del tratamiento y portabilidad escribiéndonos a <gdpr@segara.io>.

Segara considera que el compromiso con el medioambiente requiere una responsabilidad corporativa a todos los niveles, que contemple, entre otros aspectos, el respeto a la privacidad. Si se te ocurre alguna forma de mejorar la forma en que tratamos tus datos personales, no dudes en enviarnos tus sugerencias a <gdpr@segara.io>.

# Compromiso del usuario

Al contactar con nosotros, garantizas que eres mayor de 14 años y que la información facilitada es exacta y veraz.
