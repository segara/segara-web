---
title: Alberto Jiménez-Piernas
id: alberto-jimenez-piernas
slug: alberto-jimenez-piernas
image: ../../assets/images/authors/alberto-jimenez-piernas.jpeg
excerpt: CEO y Co-fundador de Segara
---

Tras una formación interdisciplinar, hice un doctorado en derecho, en Italia y en España, sobre la Responsabilidad Social Corporativa. Después, decidí emprender porque me ilusiona llevar a la práctica de las empresas la sostenibilidad. Fundé la plataforma de NegocioResponsable.Org y soy co-fundador de Segara. Comprometido con diversas asociaciones culturales, en mi tiempo libre cocino, toco la guitarra o viajo.
