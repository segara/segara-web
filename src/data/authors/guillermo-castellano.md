---
title: Guillermo Castellano
id: guillermo-castellano
slug: guillermo-castellano
image: ../../assets/images/authors/guillermo-castellano.jpg
excerpt: Director Tecnológico y Co-fundador de Segara
---

De formación humanista, me especialicé en gestión de documentos electrónicos y me dedico desde 2016 a la consultoría tecnológica y el desarrollo de software. Apuesto por el escepticismo, la transversalidad y el conocimiento abierto. Me apasionan el software libre y la historia.
