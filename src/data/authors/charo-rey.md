---
title: Charo Rey Zabalza
id: charo-rey
slug: charo-rey
image: ../../assets/images/authors/charo-rey.jpg
excerpt: Directora Científica de Segara
---

Físico de formación, forjada en la gestión de los residuos y empeñada en diseñar un futuro mejor y disfrutar con ello haciendo que las cosas pasen. Aporto todas mis competencias y mis 20 años de experiencia desde un perfil técnico sólido transversal, apasionado, colaborativo y respetuoso.
