---
title: Bienvenidos a nuestro blog sobre empresas y medioambiente
slug: bienvenida-blog-empresas-medioambiente
author: alberto-jimenez-piernas
date: 2021-04-15
image: ../../assets/images/blog/oceano-indonesia.jpg
excerpt: Con mucha ilusión os damos la bienvenida al blog de Segara Environmental Consulting y a su página web corporativa.
keywords:
  [
    "responsabilidad social corporativa",
    "huella de carbono",
    "aplicación de huella de carbono",
    "software de medición de huella de carbono",
    "impacto ambiental",
    "cambio climático",
    "gestión ambiental",
  ]
---

Con mucha ilusión os damos la bienvenida al blog de Segara Environmental Consulting y a su página web corporativa.

Somos un grupo de profesionales que siempre hemos estado relacionados de forma directa o indirecta con la **sostenibilidad**. Decidimos unir nuestras fuerzas al compartir experiencias en torno a cómo gestionar el **impacto medioambiental** de las empresas. Conocíamos de primera mano los problemas que experimentan las empresas que desean ser sostenibles, pero todavía no saben cómo puede añadir valor a sus propios procesos.

Además, las empresas interesadas en **medir, reducir y compensar** su **huella de carbono** se enfrentan a numerosos problemas prácticos. Con nuestro **software de medición de huella de carbono**, hemos hecho un esfuerzo para simplificar y añadir valor a las estrategias de **sostenibilidad medioambiental**.

![Océano de Insonesia](../../assets/images/blog/oceano-indonesia.jpg)

# ¿Por qué nos llamamos Segara?

Con el nombre de la empresa hemos querido pensar en aquellos lugares del mundo donde antes se está sufriendo el **cambio climático**. Segara, en javanés, quiere decir “mar”. La megalópolis de Yakarta, capital de Indonesia situada en la isla de Java, se hunde a un ritmo de aproximadamente 6,7cm cada año [por efecto del incremento del nivel del mar](https://www.fundacionaquae.org/el-cambio-climatico-obliga-a-trasladar-la-capital-de-indonesia-fuera-de-yakarta/). Se calcula que en el año 2050 la capital podría quedar sumergida, al menos parcialmente.

Por ello, el gobierno indonesio planea [trasladar la capital administrativa del país a otra ciudad](https://elpais.com/elpais/2019/06/07/seres_urbanos/1559896786_852411.html), posiblemente Palangkaraya. El calentamiento global no entiende de fronteras. Las reservas de metano y **CO2** que albergan los mares podrían liberarse a causa del calentamiento de las aguas y del deshielo, agravándose así la **crisis climática**.

Pero el mar es fuente de recursos pesqueros fundamentales en estas economías de subsistencia. No solamente entrañan riesgos, sino que también se asocian con la esperanza. Técnicas que hoy se utilizan en la industria extractiva podrían servir para re-inyectar **CO2** en los mares. En efecto, pueden funcionar como grandes almacenes geológicos de gases de efecto invernadero, con [un coste relativamente razonable](https://www.greenfacts.org/es/captura-almacenamiento-co2/l-2/5-almacenamiento-geologico-co2.htm).

La irreversibilidad del problema lo muestran los esfuerzos coordinados de organizaciones internacionales como la FAO y la OCDE en promover la denominada **"resiliencia al cambio climático"**. Se trata de adaptar cosechas y cultivos a las nuevas exigencias de fenómenos meteorológicos extremos cada vez más frecuentes e inevitables. Así lo incluye la Guía que han elaborado conjuntamente FAO y OCDE sobre [cadenas de suministro agrícola responsables](https://negocioresponsable.org/wp-content/uploads/2019/03/OCDE-FAO-2017-cadenas-suministro-agricola-responsables.pdf).

Nos acordamos también de quienes están padeciendo antes los efectos del **cambio climático**. No obstante, muchas otras ciudades del mundo se unirán a Yakarta. Entre las más afectadas se encontrarán Nueva York y Nueva Orleans en Estados Unidos, Guangzhou en China, Mumbai en la India, y Osaka en Japón, según diversas previsiones oficiales del [Panel intergubernamental de vigilancia sobre cambio climático](https://www.ipcc.ch/sr15/).

Por todas estas razones, la palabra Segara, que significa “mar” en javanés, nos inspiró enormemente. De alguna manera, resume el problema e inspira soluciones.

# Qué soluciones de huella de carbono aportamos

Segara ofrece una solución integral e integrada en la gestión del impacto medioambiental. Entre otras herramientas, disponemos de un **software de huella de carbono** para empresas y organizaciones. Nos permite medir, registrar, verificar, reducir y compensar la **huella de carbono**. Siempre nos referimos a “toneladas de carbono equivalente”, porque incluye todos los gases de efecto invernadero convertidos a **“CO2 equivalente”**.

El registro público del Ministerio de Transición Ecológica ofrece [un sello](https://www.miteco.gob.es/es/cambio-climatico/temas/mitigacion-politicas-y-medidas/registro-huella.aspx) que la empresa puede utilizar en su comunicación pública. Es un sello de huella de CO2 que no presenta un coste específico como el de muchos sellos de responsabilidad social empresarial de carácter privado. Con las estrategias adecuadas, puede convertirse en un instrumento más al servicio del valor de la empresa, uniendo sostenibilidad y competitividad.

![Pantallazo de SegaraCO2](../../assets/images/blog/pantallazo-segaraco2.png)

# Ser sostenible es ser competitivo

En efecto, la sostenibilidad se está convirtiendo en un vector de competitividad de las empresas, al menos a medio y largo plazo. En este blog no nos limitaremos a presentar nuestra herramienta. Queremos que sea útil para rastrear novedades que, hoy por hoy, superan con creces el paradigma de la voluntariedad en materia de responsabilidad social corporativa.

La reducción de los **gases de efecto invernadero** se ha trasladado a la agenda pública internacional y nacional, con recomendaciones como los **Objetivos de Desarrollo Sostenible** (ODS) de Naciones Unidas. No obstante, nos encontramos cada vez más con obligaciones jurídicas en materias tan dispares como la nueva taxonomía europea para finanzas sostenibles, nuevos requisitos en materia de contratación pública, e incluso bonificaciones fiscales en el horizonte de los planes de **neutralidad climática** de la Unión Europea para 2050.

Sabemos que el **impacto medioambiental** de la empresa necesita comunicarse de forma clara, entendible y transparente. Ese es nuestro compromiso en estas páginas de actualidad. Por supuesto, la interdisciplinariedad es otra de nuestras guías, para que todas nuestras publicaciones tengan el rigor científico necesario.

En este blog trataremos estas principales novedades y deseamos generar una comunidad creciente en torno al impacto positivo hacia el que debemos caminar. ¡Gracias por seguirnos!
