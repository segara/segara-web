---
title: Nueva Directiva de Informes de Sostenibilidad Corporativa (CSRD)
slug: nueva-directiva-informes-sostenibilidad-corporativa
author: alberto-jimenez-piernas
date: 2021-07-09
image: ../../assets/images/blog/nueva-directiva-sostenibilidad-corporativa.jpg
excerpt: Te contamos las principales novedades de la nueva Directiva de informes de sostenibilidad corporativa.
keywords:
  [
    "directiva de informes de sostenibilidad corporativa",
    "csrd",
    "información no financiera",
    "ley 11/2018",
    "aplicación de huella de carbono",
    "huella de carbono",
    "unión europea",
    "sostenibilidad",
    "responsabilidad social corporativa",
    "cambio climático",
  ]
---

Es probablemente la más famosa de todas las normas que han empezado a juridificar la **responsabilidad social corporativa**. La Directiva de Información no financiera y su transposición en España, la ley 11/2018, tienen los días contados. La Comisión ya prepara una **nueva Directiva de informes de sostenibilidad corporativa (CSRD)**. Te contamos las principales novedades.

# El marco actual de la información no financiera

En la actualidad las empresas de más de **250 empleados** que cumplen ciertos requisitos de facturación, de activo neto consolidado, o ser entidades de interés público, deben reportar su información no financiera. La obligación procede de la **ley 11/2018**, transposición de la [Directiva de Información No Financiera 2014/95/UE](https://negocioresponsable.org/centro-de-recursos/).

Por fin dio carta de naturaleza a las cuestiones sociales, **medioambientales** y de gobernanza, integrándolas en la rendición de cuentas anual de las empresas. Además, se hacía eco de los principales estándares “internacionalmente reconocidos” de responsabilidad social corporativa. Por último, reflejó el consenso existente en la definición de la RSC como minimización del impacto negativo de las empresas en su entorno, a diferencia de la filantropía o de la acción social.

Sin embargo, tanto la Directiva como la Ley 11/2018, no prevén sanciones específicas por incumplimiento. Solamente, de forma indirecta, el Registro Mercantil podría rechazar la inscripción de las cuentas anuales si estuviesen incompletas (sin reporte no financiero). En resumen, se ha detectado que falta un formato más estándar de reporte, y mejorar su calidad y legibilidad. Se debería incrementar la **utilidad práctica de estos informes de cara a los stakeholders e inversores**.

Además, la Ley 11/2018 nunca ha llegado a desarrollarse via reglamentaria por el legislador español, como habría sido deseable. Esto habría ayudado a las empresas a cumplir mejor con sus obligaciones. Así pues, han persistido algunas ambigüedades, o según se vea, “flexibilidades”, que la Directiva dejaba al arbitrio de los Estados miembros en su implementación:

- Dudas sobre el proceso, selección y capacitación de los “verificadores externos independientes” de los informes no financieros. ¿Cómo lo elijo? España se adelantó haciendo obligatoria esa mínima verificación externa.
- No cuenta con ningún régimen sancionador para desincentivar su incumplimiento.
- ¿Cómo elegir los estándares internacionales cuya aplicación es pertinente en mi empresa?
- ¿Qué modelo de reporte seguir? ¿Basta con la Global Reporting Initiative? ¿Puedo personalizarlo? ¿Cómo sé que lo estoy realizando correctamente?

Por todas estas razones, la Comisión Europea ha propuesto una reforma en profundidad del marco de información no financiera. El pasado 21 de abril aprobó un propuesta para una nueva Directiva de Informes de Sostenibilidad Corporativa, **CSRD** por sus siglas en inglés ([Corporate Sustainability Reporting Directive](https://ec.europa.eu/info/business-economy-euro/company-reporting-and-auditing/company-reporting/corporate-sustainability-reporting_en)).

![Nueva Directiva de Sostenibilidad Corporativa](../../assets/images/blog/nueva-directiva-sostenibilidad-corporativa.jpg){.is-image-centered}
_La burocratización y falta de estandarización de la sostenibilidad constituyen uno de los principales desafíos para las empresas obligadas. Fuente: Shutterstock._

# Enfoque general de la nueva Directiva Europea de Sostenibilidad

La Comisión Europea propone centrar los esfuerzos en la **estandarización**. La nueva directiva amplía el foco de lo **“no financiero”**. En primer lugar, la Comisión toma como referencia la **sostenibilidad** desde una óptica integral e integrada.

Se eleva el nivel de exigencia para las empresas por varias razones:

- Son más las empresas obligadas. Pasará a ser obligatorio para cualquier empresa considerada “grande” e **incluso para PYMES** que coticen en mercados de valores (con una m oratoria de 3 años para su adaptación).
- Aunque en España ya se había establecido, prevé la obligatoriedad de la **verificación externa** de la información recogida.
- Empresas subsidiarias de grupos extracomunitarios deberán también reportar en la **Unión Europea**.
- La nueva directiva incorporará un **régimen sancionador** (hoy totalmente ausente). Además, se elaborarán en colaboración con la [ESMA](https://www.esma.europa.eu/) unas guías para una mayor supervisión del contenido de los informes en el momento de su registro público, y de su adecuación a la norma.

Para garantizar la seguridad jurídica y comparabilidad de los informes, se va a avanzar en la estandarización. Esto implica que se empleará la nueva **Taxonomía de Finanzas Sostenibles de la UE** (prevista su finalización al término de 2021).

Otra novedad es que la UE creará, en colaboración con [GRI](https://www.globalreporting.org/) y [SASB](https://www.sasb.org/), un estándar obligatorio para elaboración de los **informes de sostenibilidad**. Se espera su aprobación como acto delegado en el entorno de octubre de 2022 y será desarrollado por el European Financial Reporting Advisory Group.

Por tanto, ya no habrá dudas sobre cómo elaborar los informes. Es más, el proyecto de Directiva incluye la obligación de utilizar el lenguaje **XHTML** para adaptarse a los requisitos técnicos del Punto de Acceso Único Europeo. Esta redacción deberá incluir unas etiquetas predeterminadas que detallará un reglamente ad hoc destinado a completar la directiva.

![Obligación de utilizar XHTML en los informes de sostenibilidad corporativa](../../assets/images/blog/informes-sostenibilidad-xhtml.png)
_Ejemplo de lenguaje de marcado de hipertexto sensible (XHTML) que permite el Machine Reading, como menos costes en la recopilación y comparación de reportes. Fuente de la imagen: Wikipedia._

Matrices de materialidad, tablas de GRI, diagramas de flujo y otros gráficos tendrán posiblemente menos espacio en estos informes. Se incrementa así su estandarización y rigor, de cara a inversores, stakeholders, empleados, proveedores, clientes y cualquier otro agente interesado. Pero plantea el problema de si serán atractivos y visuales para empresas orientadas al **B2C**. Éstas posiblemente tengan que elaborar otros materiales más “comunicables” de cara a los consumidores. En cualquier caso, cabía preguntarse en qué medida los informes actuales de más de 80 y 100 páginas, por muchas infografías que tengan, son realmente comunicables.

En conclusión, lo más probable es que las empresas tengan que seguir este nuevo modelo de informe en el año **2024**, referido al ejercicio de 2023. Este marco obligará al legislador español a realizar modificaciones tanto en la ley 11/2018 de Información no financiera, como en la recientemente aprobada [ley de cambio climático](https://segara.io/es/post/2021/05/nueva-ley-cambio-climatico-1/).

# Huella de carbono en la nueva Directiva de Informes de Sostenibilidad Corporativa

El medioambiente y, en concreto, la lucha contra el **cambio climático** adquiere un protagonismo nuevo en la Directiva de Sostenibilidad. Se prevé que detalle qué acciones toma la empresa respecto de su impacto en el cambio climático a nivel de organización. Este aspecto incluirá, según los informes iniciales, una verificación externa obligatoria. Desconocemos aún si esto, en la contratación pública, incluirá información sobre la **huella de carbono por proyectos**. No obstante, conviene estar preparados.

El empleo de la nueva taxonomía europea de sostenibilidad implica dedicar un espacio específico a la mitigación del cambio climático. Esto, a su vez, conlleva **reportar sobre acciones específicas y medibles de reducción y compensación de la huella de carbono**. Por este motivo será tan importante contar con herramientas tecnológicas de cara a mejorar la eficiencia de nuestro cliente interno.

![The European Green Deal](../../assets/images/blog/union-europea-directiva-sostenibilidad.jpg)
_La Unión Europea se ha erigido en un impulsor decisivo de la sostenibilidad corporativa a nivel global. El Pacto Verde europeo, o Green Deal, se está desarrollando en numerosos proyectos legislativos, como la nueva Directiva de Informes de Sostenibilidad Corporativa. Fuente: Comisión Europa._

Mantenemos nuestra recomendación de complementar el estudio de la legislación aplicable con textos de soft law (recomendaciones y guías) aunque éstos no sean vinculantes. Interesa, en particular, referirse a las [guías de la Comisión sobre reportes en materia de cambio climático](<https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52019XC0620(01)>) y otros textos que, con toda probabilidad, seguirán publicándose. Serán de ayuda en el cumplimiento de la nueva directiva de informes de sostenibilidad corporativa. Habrá que estar igualmente atentos a la aplicación y desarrollo del Reglamento y taxonomía europea de finanzas sostenibles. Ya se ha publicado un acto delegado (jurídicamente vinculante) que completa dicho Reglamento, con una [taxonomía específica en materia de adaptación y mitigación del cambio climático](<https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=PI_COM:C(2021)2800>).

La burocratización y falta de estandarización de la sostenibilidad constituyen uno de los principales desafíos para las empresas, que todavía no consiguen **enganchar sostenibilidad con competitividad**.

La **tecnología e I+D+i que aplicamos en Segara son capaces de mejorar la eficiencia** en los procesos de recopilación y tratamiento de la información, aprovechando las novedades del **data intelligence**. Así los departamentos de sostenibilidad podrán dedicar su tiempo y energía a lo que de verdad importa: añadir valor y generar ideas.
