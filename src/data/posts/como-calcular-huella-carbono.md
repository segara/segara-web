---
title: Cómo calcular la huella de carbono
slug: como-calcular-huella-carbono
author: guillermo-castellano
date: 2021-04-29
image: ../../assets/images/blog/contaminacion.jpg
excerpt: Explicamos cómo calcular la huella de carbono, conforme a los estándares internacionales, de forma clara y con ejemplos.
keywords:
  [
    "huella de carbono",
    "calcular huella de carbono",
    "impacto ambiental",
    "aplicación de huella de carbono",
    "calculadora co2",
    "huella de carbono calculo",
    "alcances huella de carbono",
  ]
---

¿Os habéis propuesto formar parte del grupo de casi 800 organizaciones que ya registran su **huella de carbono** en el Ministerio para la Transición Ecológica? Entonces quizá te estés preguntando cómo calcular la huella de carbono. En este post explicamos lo que establecen los **estándares internacionales**, de forma clara y con ejemplos prácticos.

# El indicador huella de carbono

La huella de carbono mide la contribución de una actividad al **cambio climático**. Calcularla nos permite:

1. Conocer cuántas emisiones de gases de efecto invernadero (GEI) generamos.
2. Mitigarlas a través de estrategias de reducción y/o compensación.
3. Evaluar la eficacia de las estrategias adoptadas y realizar ajustes.

Este concepto se enmarca dentro de una responsabilidad ambiental basada en datos. Como dijo el físico y matemático [William Thomson Kelvin](https://es.wikipedia.org/wiki/William_Thomson), “lo que no se mide, no se puede mejorar. Lo que no se mejora, se degrada siempre”.

Para **emisiones directas** de combustibles fósiles, el método de cálculo consiste en multiplicar las unidades de combustible utilizadas por el **factor de emisión** de ese combustible. Por ejemplo, el E5 (gasolina 95) tiene un factor de 2,18 kgCO2/l. Así, cuando en un trayecto consumimos 10 litros de E5, generamos 21,8 kg de CO2. En otras palabras, la huella de carbono del desplazamiento es igual a 21,8 kg de CO2 equivalente. La expresamos en “CO2 equivalente” porque representa la transformación de todos los tipos de GEI a CO2.

La electricidad también genera **emisiones indirectas** si no procede de fuentes 100% renovables. En esos casos se multiplica las unidades consumidas por el factor de emisión del proveedor. El cual, a su vez, se calcula a partir del combustible fósil necesario para producir cada kWh.

En definitiva, la huella de carbono permite cuantificar las emisiones de GEI generadas por un individuo, organización, proyecto, evento, producto... Gracias a ella podemos conocer el impacto de todo lo que hacemos, desde nuestros desplazamientos diarios hasta el lanzamiento de un satélite al espacio.

Dicho esto, ¿**cómo se calcula** la huella de carbono de una organización?

![Cómo calcular la huella de carbono](../../assets/images/blog/infografia-calcular-hc.png){.is-image-centered}

# Calcular la huella de carbono

Primero, se establecen los **límites organizacionales**. En este paso determinamos qué áreas de la organización se van a incluir en el cálculo. Un tema muy claro para empresas e instituciones pequeñas o medianas, pero menos evidente para organizaciones más grandes o grupos de empresas. En estos casos, antes hay que elegir entre un enfoque accionarial (la organización responde por un porcentaje de emisiones igual a sus participaciones en la sociedad) y un enfoque operativo (la organización responde por todas las emisiones de aquellas actividades sobre las que tiene un control operativo o financiero).

En segundo lugar, definimos los límites operativos, es decir, qué emisiones se incluyen. Éstas se clasifican por alcances:

- **Alcance 1**: Emisiones directas. Incluyen los **combustibles fósiles** consumidos por las instalaciones (calefacción) y vehículos de combustión interna de la organización, así como las fugas de **gases carbonofluorados** (necesarios para el aire acondicionado).
- **Alcance 2**: Emisiones indirectas derivadas del consumo de **electricidad** realizado por las instalaciones y vehículos eléctricos de la organización.
- **Alcance 3**: **Otras emisiones indirectas** (por ejemplo, desplazamientos de los empleados entre la oficina y su domicilio).

Para registrar la huella en el MITECO, se exige calcular como mínimo las emisiones de alcance 1 y 2. Tenemos que decidir si nos limitamos al alcance 1 y 2, o si también incluimos algunas emisiones de alcance 3.

Después, hay que cuantificar estas emisiones, a partir de datos medidos, calculados, estimados o asumidos. Decimos también que se realiza “el inventario de los gases de efecto invernadero” de la organización. Para ello:

1. Se contabilizan las unidades consumidas de cada combustible, fluorado y proveedor de electricidad. Esta información se suele obtener de evidencias como las facturas.
2. Se identifica el factor de emisión de cada combustible, fluorado y proveedor. Este dato se encuentra en fuentes oficiales actualizadas periódicamente.
3. Para cada combustible, fluorado y proveedor, se multiplican las unidades contabilizadas por el factor de emisión.
4. Se suman todos los parciales para obtener nuestra huella de carbono, expresada en kilogramos o toneladas de CO2 equivalente.

Imaginemos una **autoescuela** con los siguientes inventarios de gases de efecto invernadero:

- Combustible B7 (diésel) del coche de prácticas.
- Gas natural de la caldera del local.
- Fugas de gases fluorados del aire acondicionado del local.
- Electricidad consumida en el local.

Para cada uno, contabilizamos las unidades consumidas y las multiplicamos por el factor correspondiente. En el caso del coche, para obtener el total de litros repostados a lo largo del año recopilamos las **facturas de gasolina o diésel**. Suponiendo que consume 1.750 litros de B7, con un factor 2,467 kgCO2/l, obtenemos un parcial de **4.317,25 kg de CO2** (1750 \* 2,467 = 4317,25). Este proceso se repite con los demás inventarios, y sumamos los parciales.

En el caso de los automóviles de combustión interna, podemos contabilizar los **kilómetros recorridos** en lugar del combustible. Con este método, necesitamos saber las emisiones WLTP (World Harmonized Light-duty Vehicle Test Procedure) o, en su defecto, NEDC (New European Driving Cycle) del vehículo, multiplicar este valor por los kilómetros y convertir el resultado a kilogramos (este factor de emisión está expresado en gramos).

Por ejemplo, si el coche de la autoescuela recorre 35.000 km anuales, con unas emisiones de 121 gCO2/km (según ciclo WLTP), obtenemos un parcial de 4.235 kg de CO2 (35000 \* 121 / 1000 = 4235).

Finalmente, se revisan los datos y, si todo está correcto, se genera el informe de huella.

![Alcances de la huella de carbono](../../assets/images/blog/infografia-alcances.png)

# Herramientas

En todo este proceso, es importante contar con las herramientas adecuadas. El MITECO pone a disposición varias [hojas de cálculo de CO2](https://www.miteco.gob.es/es/cambio-climatico/temas/mitigacion-politicas-y-medidas/calculadoras.aspx). No obstante, con nuestra aplicación, la primera y única open source del mercado, podrás automatizar la gestión de la huella de carbono. Algunas de sus ventajas:

- Permite registrar los inventarios de forma descentralizada.
- Aprende a medida que vas introduciendo datos y te avisa en tiempo real sobre valores atípicos. Reduce errores y agiliza el proceso de revisión.
- Se integra con bases de datos de factores de emisión oficiales, incluidas la base de datos del IDAE sobre factores de emisión de electricidad y la [base de datos de vehículos de la EEA](https://www.eea.europa.eu/data-and-maps/data/co2-cars-emission-18).
- ¿Ya tenéis las evidencias en otro sistema? Nos integramos con él para que no dupliquéis trabajo.
- Obtén tu informe de huella de carbono con el nivel de agregación que necesites: por organización, instalación, flota, vehículo e, incluso, proyecto.

Si estás buscando más que una calculadora, solicita hoy mismo una demo gratuita de nuestro software.
