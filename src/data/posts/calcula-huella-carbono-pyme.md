---
title: Calcula la huella de carbono de tu PYME
slug: calcula-huella-carbono-pyme
author: guillermo-castellano
date: 2021-07-22
image: ../../assets/images/blog/pantallazo-segaraco2.png
excerpt: Hemos lanzado dos paquetes para ayudar a PYMES como la tuya a calcular su huella de carbono y ser más sostenibles.
keywords:
  [
    "huella de carbono",
    "calcular huella de carbono",
    "pymes",
    "aplicación de huella de carbono",
    "pymes por el clima",
    "calculadora co2",
    "huella de carbono calculo",
    "nueva iso 14064",
  ]
---

La transición ecológica no puede hacerse a espaldas de las **PYMES**. Por eso, desde Segara hemos lanzado dos paquetes para ayudar a pequeñas y medianas empresas como la tuya a calcular su **huella de carbono** y ser más sostenibles. Pero veamos primero por qué la huella de carbono debería interesarte.

# Beneficios de calcular la huella de carbono para las PYMES

Si no tienes claro **qué es la huella de carbono** ni cómo se calcula, te recomendamos que comiences por el [post que dedicamos a ese tema](https://segara.io/es/post/2021/04/como-calcular-huella-carbono/). ¿Vas con prisa? Quédate con que se trata del indicador más utilizado en la actualidad para conocer el desempeño ambiental de una organización o un producto.

![Cómo calcular la huella de carbono](../../assets/images/blog/infografia-calcular-hc.png){.is-image-centered}

Calcular la huella de carbono ayuda a tu PYME a ser más sostenible. Si sabes cuántas emisiones de gases de efecto invernadero (GEI) genera, puedes mitigarlas a través de estrategias de reducción y/o compensación pertinentes y medibles. Y, de esta forma, alcanzar un modelo de negocio no solo más verde, sino también más competitivo.

Hoy, más que nunca, la **sostenibilidad es un vector de competitividad**. Una sociedad cada vez más concienciada ante el **cambio climático** demanda a las empresas que incorporen a su gestión criterios de responsabilidad medioambiental. Esto es cierto con independencia de que su cliente sea:

1. El consumidor final.
2. Una gran empresa.
3. Una administración pública.

Las empresas B2C saben muy bien que la sostenibilidad de un producto o un servicio es uno de los elementos que valora el consumidor antes de adquirirlo. No hay más que ver cómo los supermercados se llenan de productos con **etiquetas ecológicas** (no siempre oficiales ni verificadas).

Si tu PYME trabaja para grandes empresas, estarás al corriente de su obligación de presentar [informes de sostenibilidad corporativa](https://segara.io/es/post/2021/07/nueva-directiva-informes-sostenibilidad-corporativa/) (lo que hasta hace poco se llamaba “información no financiera”). Lo que quizá aún no sepas es que las normas y estándares internacionales van a exigir muy pronto que tus clientes midan también sus **emisiones indirectas** de GEI. Es decir, las de proveedores como tú. Por ejemplo, las organizaciones que quieran certificarse en la **ISO 14064** a partir de 2022, deberán incluir en su huella de carbono las emisiones de su cadena de valor.

Otro ámbito donde la huella de carbono constituye una ventaja competitiva es la **contratación pública**. Cada vez más licitaciones dan puntos por medir la huella y contar con un plan de mitigación.

Esto último se suele demostrar inscribiendo la huella en el registro del Ministerio para la Transición Ecológica. La **inscripción de la huella en el MITECO** da derecho al uso de un [sello oficial y gratuito](https://www.miteco.gob.es/es/cambio-climatico/temas/mitigacion-politicas-y-medidas/que_es_Registro.aspx), muy útil tanto para licitar como para acreditar ante clientes privados el compromiso ambiental de tu PYME.

![Ejemplo de eco-etiquetado](../../assets/images/blog/eco-etiquetado.jpg)

# Presentamos nuestros paquetes para PYMES

Pese al carácter gratuito del sello del MITECO, se estima que [medir la huella de carbono cuesta entre 5.000 y 15.000 euros anuales](https://cincodias.elpais.com/cincodias/2018/06/04/companias/1528135610_781775.html). Una inversión fuera del alcance de muchas PYMES.

No puede ser que las PYMES, que representan el [97% de empresas en España](https://gdempresa.gesdocument.com/noticias/la-evolucion-de-las-pymes), se queden atrás en materia de sostenibilidad. La misión de Segara es que cualquier negocio, **sin importar su tamaño**, pueda mejorar su desempeño ambiental y distinguirse en la mitigación del cambio climático. Una buena gestión medioambiental no puede seguir siendo un lujo reservado a grandes empresas. Las PYMES se merecen contar con herramientas como SegaraCO2 y acceder al beneficio reputacional de hacerlo. Para eso apostamos por una política de precios transparente y accesible.

En este sentido, hemos diseñado dos paquetes para PYMES adaptados a su realidad y necesidades. Así, las PYMES podrán también automatizar la gestión de su huella de carbono gracias a nuestra aplicación.

## Go PYMES

Este paquete cuesta **desde 900 euros (más IVA) al año** e incluye:

- Nuestro software SegaraCO2 en servidor compartido [100% verde](https://segara.io/es/post/2021/06/centros-datos-verdes/).
- Actualizaciones periódicas del software.
- Acceso anticipado a las nuevas funcionalidades.
- Hasta 1 instalación y 1 vehículo.
- Integración con fuentes de factores de emisión oficiales, verificadas y actualizadas.
- Soporte técnico en horario de oficina.

## PYMES por el clima

Este paquete cuesta **desde 3.000 euros (más IVA) al año**. Incluye todo lo del paquete Go PYMES y, además:

- Hasta 2 instalaciones y 5 vehículos.
- Plan de mitigación personalizado.
- Gestión de la inscripción de la huella en el MITECO
- Gestión de la solicitud de la [ayuda para la responsabilidad social y la conciliación laboral en la Comunidad de Madrid](https://administracion-electronica.comunidad.madrid/node/213249).
- 20% de descuento a partir del segundo año.

Si tu PYME desarrolla su actividad en la **Comunidad de Madrid**, puedes acogerte a una **subvención que cubre hasta el 100% del coste de obtención de la certificación**. Aunque lógicamente no podemos garantizártela, gestionaremos tu solicitud de principio a fin y nos esforzaremos al máximo para que sea aceptada.

![Pantallazo de nuestra aplicación de huella de carbono](../../assets/images/blog/pantallazo-segaraco2.png)

Estas ofertas son para empresas de **hasta 50 trabajadores y 10 millones de euros de facturación**. Para empresas más grandes, tenemos otras tarifas, que pronto publicaremos también. Mientras tanto, puedes consultarnos por correo electrónico sin ningún problema.

Algunas de las ventajas de trabajar con nuestro software:

- Permite registrar los inventarios de forma descentralizada.
- Aprende a medida que vas introduciendo datos y te avisa en tiempo real sobre valores atípicos. Reduce errores y agiliza el proceso de revisión.
- Se integra con bases de datos de factores de emisión oficiales, incluidas la base de datos del IDAE sobre factores de emisión de electricidad y la [base de datos de vehículos de la EEA](https://www.eea.europa.eu/data-and-maps/data/co2-cars-emission-18).
- ¿Ya tenéis las evidencias en otro sistema? Nos integramos con él para que no dupliquéis trabajo.
- Obtén tu informe de huella de carbono con el nivel de agregación que necesites: por organización, instalación, flota, vehículo e, incluso, proyecto.

Si estás buscando más que una calculadora, solicítanos hoy mismo una demo gratuita.
