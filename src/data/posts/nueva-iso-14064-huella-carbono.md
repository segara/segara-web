---
title: La nueva ISO 14064 de huella de carbono
slug: nueva-iso-14064-huella-carbono
author: alberto-jimenez-piernas
date: 2021-08-05
image: ../../assets/images/blog/huella-de-carbono-iso-26000.png
excerpt: Te contamos las principales novedades de la nueva ISO 14064 de huella de carbono.
keywords:
  [
    "iso 1406",
    "aplicación de huella de carbono",
    "huella de carbono",
    "sostenibilidad",
    "responsabilidad social corporativa",
    "cambio climático",
    "gestión ambiental",
    "calculadora huella de carbono",
    "nueva iso 14064",
    "novedades iso 14064",
  ]
---

La norma **ISO 14064** sobre huella de carbono se ha actualizado en 2019 y ya ha sido traducida por el Comité técnico de AENOR. La nueva ISO 14064-1:2019 **amplía requisitos para emisiones indirectas**. El período de adaptación finaliza en diciembre y se aplicará desde enero de 2022. En este post resumiremos en qué afecta a las empresas y organizaciones.

# Responsabilidad ambiental basadas en estándares

Conviene recordar que las iniciativas de reducción del cambio climático se enmarcan dentro de la dimensión medioambiental de la Responsabilidad Social Corporativa (RSC). **ISO 26000 es el principal estándar privado internacional en materia de RSC**. La define como la minimización del impacto negativo de las actividades de la empresa. Entre sus “materias fundamentales” aparece lógicamente el medioambiente, así como la dimensión sociolaboral, los derechos humanos o la gobernanza corporativa.

Dadas las infinitas particularidades de empresas y organizaciones, desde el primer momento ISO optó por una **guía no certificable de RSC (ISO 26000)**. Por su parte, la serie 14000 sobre medioambiente sí está compuesta por estándares certificables, en tanto que constituyen “sistemas de gestión” propiamente dichos.

Dicho esto, el propio ISO 26000 constituye un punto de partida importante. En su **apartado 6.5.5. se ocupa específicamente de la gestión de las emisiones de gases de efecto invernadero**. Distingue claramente entre emisiones directas de la organización, e indirectas. Da prioridad igualmente a reducir las emisiones propias, antes de implementar otras estrategias.

Respecto de las emisiones restantes (“remaining GHG Emissions”) subraya la importancia de realizar programas fiables y transparentes de reducción de emisiones (“reliable emissions reduction programmes that operate in a transparent way”). Por esta razón, **Segara realiza un proceso independiente de verificación de sus aliados de reducción y compensación**.

![Materias fundamentales de la RSC conforme a ISO 26000](../../assets/images/blog/huella-de-carbono-iso-26000.png)
_Las materias fundamentales de la RSC conforme a ISO 26000. El esquema “Plan-Do-Check-Act” es común a la mayoría de los sistemas de gestión. Fuente: Elaboración propia._

# Principios generales de la nueva ISO 14064

El universo de estándares ISO de la serie 14000 sobre medioambiente ha pasado por un proceso de actualización reciente. En este sentido, destacan las actualizaciones de la nueva ISO 14064, que debería utilizarse conjuntamente con la ISO 14065 y la ISO 14063. En la medida que lo permita cada organización, en aras de alcanzar la excelencia lo ideal es conectarlo asimismo con los [sistemas de calidad total](https://segara.io/es/post/2021/06/calidad-total/). El compromiso de Segara es basarnos siempre en los más importantes y recientes estándares internacionalmente reconocidos.

Los principios generales que deben guiar el registro y reporte de emisiones de Gases de Efecto Invernadero (GEI) son (UNE-EN ISO 14064-1:2019, págs. 18 y 19):

- **Pertinencia**. Las emisiones deben relacionarse con las **actividades propias de la empresa** u organización, con datos reales de fuentes, sumideros, datos obtenidos, metodología en el tratamiento de dichos datos.
- **Integridad**. Se refiere a su vocación de reflejar fielmente la totalidad de las emisiones GEI. La estructura modular de Segara contempla **todos los inventarios de GEI** y la posibilidad de añadir especificidades.
- **Exactitud**. La precisión en las mediciones es fundamental. La principal contribución de Segara en este aspecto son sus **avisos automáticos de desviación** para valores inusualmente altos o bajos. Además, nos conectamos siempre con **fuentes oficiales de factores de emisión** para garantizar la robustez científica de nuestros resultados.
- **Transparencia**. El informe final de nuestra aplicación de huella de carbono utiliza un lenguaje accesible y comprensible que ponga en valor los resultados. Esto mejora la fluidez y fiabilidad de la comunicación con stakeholders e inversores.
- **Coherencia**. Es fundamental para permitir comparativas rigurosas y significativas. Por eso, Segara es más que una **calculadora de huella de carbono**. Ofrecemos **comparaciones** a partir del año base pero también por sector económico y por unidades de facturación

En relación con esta coherencia, la organización debe seleccionar un año base que sirva de referencia para el evolutivo de su desempeño medioambiental. Se destaca que deberá incluir una “explicación de cualquier cambio en el año base” a fin de que no se vea afectada la “comparabilidad resultante de dicho nuevo cálculo” (requisitos (k) y (l), UNE-EN ISO 14064-1:2019, pág. 28). Esto dificultará la manipulación de los resultados evolutivos.

Antes de generar un informe, nuestra aplicación de huella de carbono obliga al usuario/a a seleccionar dicho año base. Nuestros informes, además, reflejan en su metodología los límites organizacionales y operativos, que ahora se llaman “límites de informe”, tal y como exige el apartado 5 de la nueva ISO 14064-1.

Por último, se establece el siguiente inventario de categorías de GEI:

- Emisiones y remociones directas de GEI
- Emisiones indirectas de GEI por energía importada
- Emisiones indirectas de GEI por transporte
- Emisiones indirectas de GEI por productos utilizados por la organización
- Emisiones indirectas de GEI asociadas con el uso de productos de la organización;
- Emisiones indirectas de GEI por otras fuentes.

![Novedades ISO en materia de huella de carbono](../../assets/images/blog/nueva-iso-14064.png)
_Novedades ISO en materia de huella de carbono. Fuente: Elaboración propia._

# Un nuevo requisito: las emisiones indirectas

La anterior versión de ISO 14064 se refería solamente a “otras emisiones indirectas de GEI”. Ahora pasan a denominarse “emisiones indirectas de GEI” y se subcategorizan con mayor detalle. Como es sabido, pueden corresponderse tanto a los alcances 2 y 3 del GHG Protocol. La principal novedad es que, para superar una verificación externa, **ahora figura como un “requisito” reportar sobre “emisiones indirectas cuantificadas de GEI”** (apartados 5.2.3 y 9.3.1 de la nueva ISO 14064) siempre que sean “significativas”.

Con el objeto de seleccionar qué emisiones indirectas se consideran significativas, cada organización debe utilizar y justificar sus propios criterios. A estos efectos puede servir de orientación el Anexo H de la nueva ISO 14064. Hay que valorar la magnitud de esas emisiones indirectas, el coste de obtener datos pertinentes y fiables, cuán necesario es (o va a ser) de cara a sus posibles destinatarios (stakeholders, inversores y reguladores).

Este requisito de las emisiones indirectas significativas propiciará que las grandes empresas impulsen la sostenibilidad en sus cadenas de valor. Por tanto, consideramos un acierto estos cambios, que ayudarán a [extender a las PYMES la lucha contra el cambio climático](https://segara.io/es/post/2021/07/calcula-huella-carbono-pyme/). En última instancia, el alcance 3 de una gran empresa es, fundamentalmente, los alcances 1 y 2 de sus proveedores.

Mientras finaliza el período de adaptación hasta 2022, recomendamos empezar a trabajar en la identificación, metodología y criterios que determinen la significancia de las emisiones indirectas de alcance 3. Ya no bastará con los alcances 1 y 2. La ventaja de herramientas avanzadas como Segara es que permite integrar y automatizar los registros de los proveedores (sus alcances 1 y 2) con los de sus clientes (su alcance 3).
