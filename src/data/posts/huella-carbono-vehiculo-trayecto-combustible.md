---
title: Huella de carbono de un vehículo, ¿por trayecto o por combustible?
slug: huella-carbono-vehiculo-trayecto-combustible
author: guillermo-castellano
date: 2021-05-13
image: ../../assets/images/blog/comparativa-consumos-seat.png
excerpt: Explicamos los dos métodos para calcular la huella de carbono de un vehículo de combustión interna, por kilómetros recorridos y por litros de combustible consumidos.
keywords:
  [
    "huella de carbono",
    "calcular huella de carbono",
    "huella de carbono calculo",
    "aplicación de huella de carbono",
    "vehículo de combustión interna",
    "nedc",
    "wltp",
    "calculadora co2",
    "emisiones",
  ]
---

Existen dos métodos para calcular la huella de carbono de un **vehículo de combustión interna**: por kilómetros recorridos y por litros de combustible consumidos. Ya [explicamos ambas opciones en otro post](https://segara.io/es/post/2021/04/como-calcular-huella-carbono/), pero nos dejamos dos cuestiones importantes. ¿Cuál es más preciso? Y, sobre todo, ¿cómo puedes determinar el más conveniente para tu organización?

# Trayecto o combustible: ¿cuál es más preciso?

El cálculo por trayecto se basa en la estimación de cuántas emisiones genera el vehículo por cada **kilómetro**, mientras que el cálculo por combustible se basa en los **litros** efectivamente utilizados. Un vehículo térmico genera emisiones de CO2 al quemar **combustible fósil** (gasolina, diésel o gas natural compromido), así que el cálculo de la huella de carbono será siempre más preciso mediante el segundo método.

La pregunta clave es cuánta diferencia de precisión existe, en la práctica, entre ambos. Para responderla, veamos cuáles son las diferencias a los 100 km en un utilitario. Concretamente, el SEAT Ibiza 1.0 MPI 59 kW (80 CV) S&S Reference del 2020.

Los datos que necesitamos de este coche son:

- Combustible: E5 ó E10.
- Consumo medio homologado: 5,5 l/100km.
- Emisiones (WLTP): 122 gCO2/km.
- Emisiones (NEDC): 108 gCO2/km.

De los dos combustibles que puede utilizar, en la comparativa nos ceñimos al más habitual, el E5 (gasolina 95). Éste tiene un factor de emisión de 2,18 kgCO2/l. Recordemos que esto significa que, al quemar 1 litro de E5, emitimos a la atmósfera 2,18 kg de CO2.

En el método por trayecto, multiplicamos los kilómetros recorridos (100) por las emisiones del vehículo (122, por el ciclo WLTP, y 108, por el NEDC), y convertimos el resultado a kilogramos (dividiéndolo por 1.000). Esto da una **huella de carbono** de 12,2 ó 10,8 kg de CO2 equivalente.

En el método por combustible, multiplicamos el factor de emisión del E5 (2,18) por el total de litros consumidos en 100 km. A una media de 5,5 l/100km, da 11,99 kg. Como los litros varían en función del tipo de conducción, repetimos este cálculo con los distintos consumos posibles (en este caso, la horquilla 4,7 a 6,3). Obtenemos unas emisiones entre 10,25 y 13,73 kg de CO2 equivalente.

![Comparativa de consumos de un SEAT Ibiza](../../assets/images/blog/comparativa-consumos-seat.png)

Como anticipábamos, el cálculo por kilómetros arroja un resultado estático. En cambio, el cálculo por litros da un resultado dinámico, que varía en función del consumo medio alcanzado por el conductor.

Observamos también una diferencia de 1,4 kg de CO2 entre las emisiones WLTP y NEDC. Esto se debe a que el estándar [Nuevo Ciclo de Conducción Europeo (NEDC)](https://es.wikipedia.org/wiki/New_European_Driving_Cycle) es menos preciso que el [World Harmonized Light-duty Vehicle Test Procedure (WLTP)](https://es.wikipedia.org/wiki/WLTP), basado en situaciones reales. No nos extraña, entonces, que las emisiones estimadas por el procedimiento WLTP coincidan con las emisiones de un consumo real de 5,6 litros a los 100 km, solo 0,1 litros por encima del consumo medio homologado del vehículo.

De hecho, este ejemplo nos muestra que **ambos métodos ofrecen una precisión similar** si:

1. Las emisiones del vehículo han sido obtenidas por el procedimiento WLTP. Condición que se cumple en todos los vehículos homologados en la Unión Europea a partir del 1 de septiembre de 2018.
2. El vehículo tiene unos consumos reales alrededor de su consumo medio homologado. Condición que, para utilitarios, suele cumplirse cuando realizamos conducciones mixtas (ciudad y carretera) a velocidades legales.

# Entonces, ¿cuál es más conveniente?

Si, en los escenarios más frecuentes, ambos métodos ofrecen una precisión similar, ¿qué **criterios** utilizamos para elegir uno? Hablamos de criterios porque no existe un método más conveniente que otro, sino circunstancias que pueden aconsejar uno u otro.

El cálculo por combustible es el más fácil de implementar cuando ya disponemos de las facturas de combustible registradas en un sistema informático. Además, dado que sabemos el importe de cada factura, podemos conocer no solo la huella de carbono, sino los costes económicos asociados.

El cálculo por trayecto puede ser interesante si ya registramos en algún sistema los kilómetros realizados por los vehículos de nuestra flota. Dependiendo de la calidad de los datos, podremos obtener información como la distancia habitual de los trayectos, las rutas más frecuentes o el tipo de conducción realizado con cada vehículo (ciudad, carretera o mixto). Conocimiento muy valioso para mejorar la eficiencia de nuestro transporte.

Ambos métodos pueden ser muy útiles. Por eso, nuestra **aplicación de huella de carbono** te permite calcular las emisiones de tu flota de vehículos tanto por litros consumidos como por kilómetros recorridos.

En este pantallazo vemos cómo funciona el registro de facturas de combustible.

![Pantalla de registro de facturas de combustible](../../assets/images/blog/segaraco2-facturas.png)

Y, en este otro, vemos el registro de trayectos.

![Pantalla de registro de trayectos](../../assets/images/blog/segaraco2-trayectos.png)

Para obtener el parcial de los trayectos, nuestro módulo de cálculo **prioriza el cálculo por emisiones WLTP** si está disponible, como puedes [comprobar en el código fuente](https://gitlab.com/segara/segara-co2-api/-/blob/master/app/calculator.py). Las emisiones WLTP son más precisas que las obtenidas por el procedimiento NEDC y hemos diseñado nuestro software para que utilice éstas últimas solo si no hay datos mejores.

Vemos también que cada factura o trayecto se asocia un vehículo. Esto permite **conocer las emisiones a nivel de vehículo, flota, tipo de vehículo, etc.**

Además de ofrecer ambos métodos a nuestros clientes, los ayudamos a determinar, con fundamento,cuál da más valor a su organización. No somos solo desarrolladores. Nuestro equipo está formado por **especialistas en medioambiente y responsabilidad social corporativa** con experiencia contrastable en el sector.

¿Te interesa? Solicita hoy mismo una demo gratuita de nuestra plataforma.
