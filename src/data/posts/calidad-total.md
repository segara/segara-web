---
title: ¿Hacia dónde dirijo la calidad de mi empresa?
slug: calidad-total
author: charo-rey
date: 2021-06-10
image: ../../assets/images/blog/calidad-total.png
excerpt: Explicamos para qué sirve realmente un sistema de calidad y si éste siempre tiene que estar certificado para que sea válido o si sólo hay un camino para afrontar un sistema de la calidad.
keywords:
  [
    "sistema de calidad",
    "calidad total",
    "iso 9001",
    "efqm",
    "huella de carbono",
    "cambio climático",
    "gestión ambiental",
    "sistema de gestion ambiental",
  ]
---

El post de hoy va de explicar para qué sirve realmente un **sistema de calidad** y si éste siempre tiene que estar certificado para que sea válido o si sólo hay un camino para afrontar un sistema de la calidad.

Aparentemente pueden parecer obviedades para las que todos sabemos las respuestas, pero no es así.

# Para qué sirve un sistema de calidad

Empezando por el principio, ¿por qué las empresas implantan un sistema de la calidad, lo certifican y gastan recursos en hacerlo? ¿Es realmente útil?

Seguro que podríamos encontrar tantas respuestas como organizaciones, pero algunas de las razones más extendidas son que lo impone la **competencia**, el sector o la actividad. En menos ocasiones, las organizaciones y sobre todo la alta dirección creen firmemente en los beneficios de la calidad. Y, por último, también con frecuencia la implantación de un sistema de la calidad está asociada a la creencia de que ayuda a hacer las cosas mejor garantizando el mejor resultado posible.

Ummmm... ¿es eso cierto? Sólo en parte. Tener un sistema de gestión de la calidad **no garantiza en absoluto que tu producto/servicio sea el mejor, sino que sigue una estandarización** que la propia organización se autoimpone **y que se establecen las herramientas para detectar desviaciones** respecto a lo establecido. Así nuevamente habrá un sistema de calidad diferente por cada organización. O debería ser así, aunque la realidad es tozuda y solemos encontrar sistemas encorsetados que se replican en muchas organizaciones diferentes sin adaptarlos a su propia idiosincrasia. Esto tendría mucho que ver con la consultoría que debe aportar valor, aunque eso será para otro día... Por último está la razón de creer firmemente en hacer las cosas siempre de una forma determinada garantizando la reproducibilidad que eso conlleva.

Una vez implantado un sistema de gestión de la calidad, la decisión de **certificarlo** o no depende de las circunstancias y necesidades de la organización y en ese punto habrá que contar con una partida presupuestaria para afrontar los servicios de un ente de certificación externo (habitualmente en ciclos de 3 años).

# El concepto de calidad total

Ahora bien, no es éste el único camino para afrontar la calidad y aquí entra el concepto de **calidad total**. A él se acercan organizaciones que seguramente tienen una madurez alta y una sólida conciencia respecto a sus empleados, partes interesadas y la propia sociedad. Fijaos, se trata de una auténtica filosofía de gestión en la que **la calidad deja de ser un departamento estanco y pasa a ser parte del ADN de la organización**, de su cultura desde el momento en que todos participan en el proceso.

Seguro que habréis visto el gráfico de la **evolución** de la calidad.

![Evolución de la calidad](../../assets/images/blog/evolucion-calidad.png)

La calidad total se refiere a la gestión de la calidad respecto a los pasos precedentes (inspección de producto, control estadístico de procesos, aseguramiento de la calidad, calidad total).

En este caso, además, es la propia organización la que se **evalúa** a sí misma. ¡Esto sí que es interesante! ¿Sería tu organización capaz de hacerlo sin trampas para salir bien en la foto?

Una manera de llegar a la calidad total es pasar primero por la madurez de sistemas de gestión apoyados en las normas **ISO 9001** y luego dar el salto, pero ojo porque se trata de algo realmente comprometedor desde el punto de vista de los recursos, las propias implicaciones y la propia cultura/filosofía de la empresa. En cualquier caso, el concepto de calidad total es para organizaciones que se quieren distinguir por la excelencia.

Dependiendo de la zona geográfica en la que se encuentre la organización hay 4 modelos diferentes:

1. Deming Prize, para Japón.
2. Malcolm Baldrige, para Estados Unidos.
3. **EFQM**, para Europa.
4. FUNDIBQ, para Iberoamérica.

Ya profundizaremos.

![Concepto de calidad total](../../assets/images/blog/calidad-total.png)

Todas las soluciones son válidas independientemente del origen de la necesidad siempre que se hagan desde la profesionalidad. Y, sobre todo, no para cubrir el expediente, sino aportando a los procesos y a todos los actores implicados la esencia de la calidad independientemente del certificado final. Éste no debería de ser un fin en sí mismo sino un mecanismo para una mejora continua.

# Calidad y huella de carbono

¿Sabéis cómo se refiere la [Asociación Española para la Calidad](https://www.aec.es/) a la [huella de carbono](https://segara.io/es/post/2021/04/como-calcular-huella-carbono/)? Ahí va:

“La **Huella de Carbono** (HDC) se refiere al impacto ambiental que se produce como consecuencia de la generación de emisiones de gases de efecto invernadero (GEI). Implica el cálculo de las emisiones de los GEI **asociadas con una organización, actividad o ciclo de vida de un producto o servicio**.

La Huella de Carbono (HDC) permite comprobar y gestionar las emisiones de los **Gases de Efecto Invernadero** (GEI) a lo largo de la cadena de suministro, y por lo tanto, se permite realizar una comparación de la HDC entre los productores y productos ubicados en distintos lugares. Al proporcionar información sobre los productos, la HDC permite a las empresas ofrecer una 'imagen más verde', ya que pueden publicitar los esfuerzos que realizan para combatir el problema del calentamiento global, así como fomentar la sensibilización de los consumidores sobre las emisiones de GEI.”

Habla de procesos, productos, servicios, imagen, cadena de suministro... En fin, calidad.

Por nuestra parte vamos a empezar nuestro camino incorporando a nuestra cultura el compromiso por la calidad total. Ya os contaremos cómo nos va. Sois parte interesada.
