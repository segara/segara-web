---
title: Centros de datos verdes
slug: centros-datos-verdes
author: guillermo-castellano
date: 2021-06-29
image: ../../assets/images/blog/alpes.jpg
excerpt: Hoy hablamos de las emisiones asociadas a los sistemas informáticos y de por qué en Segara alojamos todos nuestros servidores en centros de datos verdes.
keywords:
  [
    "aplicación de huella de carbono",
    "centros de datos verdes",
    "huella de carbono",
    "alcances huella de carbono",
    "energías renovables",
    "hosting verde",
    "impacto ambiental",
  ]
---

Con el avance de la transformación digital, los **centros de datos verdes** juegan un papel cada vez más importante para reducir la **huella de carbono**. Sin ellos nunca será cierto del todo que la digitalización tiene un impacto ambiental positivo, solo porque elimine papel. Hoy hablamos de las emisiones asociadas a los sistemas informáticos y de por qué en Segara alojamos todos nuestros servidores en centros de datos verdes.

# Emisiones de CO2 de los datacenters

El pasado 30 de marzo, el [Gobierno de España anunció la aprobación del Reglamento para la actuación y funcionamiento del sector público por medios electrónicos](https://portal.mineco.gob.es/es-es/comunicacion/Paginas/210330_np_electronico.aspx). En el comunicado oficial, el Ministerio de Asuntos Económicos y Transformación Digital destacó que el reglamento tiene “un impacto ambiental positivo al reducir la huella de carbono de la tramitación tradicional en la que se emplea masivamente el papel, la impresión láser y la logística”. Esta clase de afirmaciones se ha convertido en un argumento recurrente para defender la **transformación digital**. ¿Qué hay de cierto en ellas?

El impacto ambiental del **papel** es bien conocido. Se estima que una tonelada de papel implica la tala de 15 árboles y en consumo de 150.000 a 200.000 litros de agua. Sin contar con las emisiones de CO2 que generan su transporte, producción y posterior reciclaje. La documentación digital ahorra papel, reduce los metros lineales de almacenamiento necesarios y previene gastar combustible transfiriendo documentación de un archivo a otro.

Sin embargo, los sistemas informáticos en los que se basa esta transformación digital también generan un **impacto ambiental** durante todo su **ciclo de vida**. La alimentación de los servidores requiere energía, que mayormente procede de combustibles fósiles. Y, previamente a su uso, estos servidores se transportan del lugar donde se producen al centro de datos, también en vehículos movidos mediante combustibles fósiles. Y, antes de transportarlos, son manufacturados, para lo cual se emplea aún más energía y se obtienen, previamente, las materias primas. Además, al final de su vida útil, el hardware se convierte en residuos que hay que gestionar.

Como usuarias de estos servidores, las organizaciones somos responsables, sobre todo, de su uso. Mantenerlos encendidos consume electricidad. Y, [como explicamos en otro post](https://segara.io/es/post/2021/04/como-calcular-huella-carbono/), la electricidad genera **emisiones indirectas** si no procede de fuentes 100% renovables.

En el caso de que la organización posea su propio datacenter, estas emisiones indirectas forman parte de sus inventarios de **alcance 2**. Es decir, son emisiones indirectas sobre las que la organización tiene control directo.

Si la organización ha externalizado su infraestructura informática en una tercera empresa (lo que se conoce como “nube”), estas emisiones indirectas forman parte de sus inventarios de **alcance 3**. Son emisiones indirectas sobre las que la organización no tiene control directo y que actualmente no es obligatorio incluir en el cálculo de la huella de carbono presentado al MITECO.

En ambos casos, se trata de emisiones generadas por la actividad de nuestra organización. Como tales, afectan a nuestro desempeño ambiental y se recomienda medirlas y mitigarlas.

![Alcances de la huella de carbono](../../assets/images/blog/infografia-alcances.png)

# La importancia de los centros de datos verdes

El impacto ambiental de los servidores varía en función de la vida útil y la eficiencia energética del propio hardware, el origen de la electricidad y las técnicas de enfriamiento implementadas. No todos los centros de procesamiento de datos lo hacen igual de bien en estos tres indicadores, y por eso existen opciones de alojamiento más sostenibles que otras.

Para reducir la huella de carbono de nuestra organización, conviene alojar nuestros sistemas en centros de datos alimentados con **energías renovables**. Estos suelen anunciar en sus materiales publicitarios que son sostenibles. Extremo más fácil de verificar si el centro mide su huella de carbono y/o cuenta con certificaciones relacionadas con eficiencia energética.

En Segara alojamos la mayoría de nuestros servidores, tanto de desarrollo como de producción, en [Data Center Light](https://datacenterlight.ch/en-us/cms/hydropower/). Este centro de datos, ubicado en los Alpes glaroneses, cuenta con su propia **planta hidroeléctrica** (movida por el [río Linth](https://es.wikipedia.org/wiki/R%C3%ADo_Linth)), gracias a la cual genera el 99,9% de la energía que necesita. El 0,01% restante lo obtiene de plantas solares.

Además de funcionar solamente con energías renovables, este centro se encuentra en Suiza, uno de los Estados más garantistas del mundo en materia de seguridad y privacidad.

![Fotografía del monte Cervino, en Los Alpes](../../assets/images/blog/alpes.jpg)

[Webo.hosting](https://webo.hosting/), donde alojamos algunos de nuestros servicios internos, también alimenta sus máquinas con energía 100% renovable.

Al cuidar su **cadena de valor**, las empresas mejoran sus indicadores ambientales y ofrecen un servicio más atractivo a sus clientes. A medida que aumenta la conciencia ambiental del consumidor, la sostenibilidad se convierte en un vector de **competitividad** cada vez más importante.

¿Sabes cómo puede distinguirse tu organización en la mitigación del cambio climático? Con nuestra aplicación no solo podrás automatizar la gestión de tu huella de carbono. Su enfoque a **inteligencia de negocio** os ayudará a reducir costes, diseñar planes de mitigación eficaces y comunicar mejor a los stakeholders vuestro compromiso ambiental. Solicita hoy mismo una demo gratuita.
