---
title: Nueva ley de cambio climático (parte 1)
slug: nueva-ley-cambio-climatico-1
author: alberto-jimenez-piernas
date: 2021-05-27
image: ../../assets/images/blog/segara-CO2-emisiones.jpg
excerpt: Primera parte de una serie dedicada a analizar la recién aprobada Ley 7/2021 sobre cambio climático y transición energética.
keywords:
  [
    "ley de cambio climático",
    "responsabilidad social corporativa",
    "huella de carbono",
    "gases de efecto invernadero",
    "aplicación de huella de carbono",
    "unión europea",
    "sostenibilidad",
    "impacto ambiental",
    "finanzas sostenibles",
  ]
---

La dimensión medioambiental ha sido históricamente la más desarrollada de la **responsabilidad social corporativa** (RSC) por varias razones. En primer lugar, por su propia naturaleza, facilita calcular los retornos de las inversiones y mejoras introducidas por la empresa. En otras áreas de la RSC buscamos a menudo evitar riesgos de difícil cuantificación (al menos, hasta que se producen). En materia medioambiental, los retornos a medio y largo plazo son más sencillos de calcular, como por ejemplo en el caso de la introducción de luces LED, de la reducción de uso de papel, la gestión más eficiente de residuos, o cambios de las fuentes de energía.

Además, la protección del **medioambiente** ha sido la punta de lanza de la introducción de nuevas temáticas en la agenda de los departamentos legales. El medioambiente ha irrumpido con fuerza en la gestión legal, antes dedicada casi exclusivamente a la triada fiscal-laboral-mercantil. En este sentido, ha sido la avanzadilla de la juridificación progresiva de muchos aspectos de la responsabilidad social empresarial.

Por todo ello, no debe sorprender a ningún observador atento de esta realidad, la reciente aprobación, el pasado 20 de mayo de 2021, de la nueva **ley de cambio climático**.

# Contexto para la interpretación de la nueva ley de cambio climático

Si bien dedicaremos posts adicionales aclarando esta nueva ley, es muy importante interpretarla en su contexto adecuado.

Esto pasa por remitirnos en primer lugar al escenario **internacional**. La ley española
de cambio climático traslada al ordenamiento jurídico interno las obligaciones adquiridas por España en el [Acuerdo de París de 2015](https://unfccc.int/es/process-and-meetings/the-paris-agreement/el-acuerdo-de-paris).

Se trata de un tratado internacional que, en principio, obliga a los Estados que lo han ratificado a limitar el calentamiento global a entre 1.5 y 2 grados Celsius de aumento de la temperatura. Para ello, se deben reducir paulatinamente las **emisiones de gases de efecto invernadero**. El propio Acuerdo destaca tres herramientas:

1. Facilidades financieras para Estados en desarrollo.
2. Desarrollo de capacidades de medición, reducción y mitigación de emisiones
3. Tecnología e innovación.

El **software de Segara** representa una contribución a la I+D+i en este ámbito para ayudar a las empresas a gestionar estos objetivos.

Por otro lado, debemos destacar el marco de la **Unión Europea**. Hace muy poco, los días 23 y 24 de mayo el Consejo de la Unión Europea, que agrupa a los jefes de gobierno de los Estados miembros, celebraron una sesión extraordinaria sobre el clima. Conecta con un primer borrador acordado entre el propio Consejo y el Parlamento Europeo de proyecto de ["Ley Europea sobre el Clima"](https://www.consilium.europa.eu/es/press/press-releases/2021/05/05/european-climate-law-council-and-parliament-reach-provisional-agreement/), aún en proceso de negociación. Por el momento y a falta de su aprobación final, todo apunta a la neutralidad climática de cara a 2050 y al objetivo de reducir un 55% las emisiones en 2030 respecto de los niveles del año 1990.

Por supuesto, estos planes legislativos europeos guardan coherencia con los recientemente aprobados Marco Financiero Plurianual 2021-2027 y con el [Plan de Recuperación para Europa](https://ec.europa.eu/info/strategy/recovery-plan-europe_es) post-coronavirus. Cuenta con tres pilares:

1. Financiación (sin precedentes) para la competitividad.
2. Digitalización e innovación.
3. Sostenibilidad socio-ambiental.

# Primeras claves de la ley de cambio climático

La [Ley 7/2021 sobre cambio climático y transición energética](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2021-8447) aprobada este mes de mayo, fija el objetivo de reducir las emisiones un 23% en 2030 respecto de los registros de 1990. Sus prioridades a corto-medio plazo son tres:

1. La electrificación de la movilidad
2. El aumento de la parte de energía que procede de origen renovable,
3. La eficiencia energética para reducir el consumo.

![Pantallazo de SegaraCO2](../../assets/images/blog/huella-de-carbono-coches-electricos.jpg)
_La electrificación de la movilidad es una de las prioridades de la nueva ley._

En otro orden de cosas, permanece inalterado el conocido principio "quien contamina, paga" por lo que se prevé un encarecimiento de los derechos de emisión. En términos generales, esto hará menos **competitivas** a las empresas que ni reduzca sus emisiones satisfactoriamente.

No se debe descartar un desarrollo más específico de aspectos concretos de esta ley por la vía reglamentaria. Por el momento prevé la redacción de dos textos de _soft law_ (recomendaciones no vinculantes y planes), sobre los que se apoyará su implementación: el Plan Nacional Integrado de Energía y Clima, y la Estrategia para la Descarbonización de la Economía a 2050, dos textos que aún deben ver la luz.

La Ley promueve la utilización de indicadores precisos, con base científica, como la **huella de carbono**. Solo midiendo correctamente, cada empresa será capaz de conocer su desempeño respecto de años anteriores, compararse con las emisiones del sector al que pertenece, o dar a conocer a un cliente la huella de carbono estimada de un proyecto concreto.

Por último, otras novedades destacadas se refieren a la prohibición del _fracking_ o fractura hidráulica, de la concesión de nuevas prospecciones de hidrocarburos, así como la inclusión de la **sostenibilidad** y el cambio climático en los planes de estudios a distintos niveles, aún por especificar.

# Qué significa para las empresas

Desarrollaremos con mayor detalle las novedades normativas y políticas en esta temática para que nuestros clientes y lectores no se queden descolgados. Extraemos algunas primeras conclusiones importantes para las empresas.

## 1) Las nuevas exigencias han venido para quedarse

Independientemente de la fluidez de la política nacional, el cambio climático está en la agenda internacional y europea a corto, medio y largo plazo. No resulta aconsejable que la empresa especule con cambios de gobierno, que pueden introducir matices pero será muy improbable que reviertan este escenario internacional y europeo cada vez más exigente con la reducción de las emisiones de gases de efecto invernadero.

## 2) Sectores y modelos de negocio más afectados

Aunque se extenderá a toda clase de empresas, los sectores que deben ocuparse de forma inmediata de estas adaptaciones son los de la **energía**, el **transporte**, la **logística**, el **agroalimentario**, y **construcción e infraestructuras**. Para estos sectores no sería prudente esperar, sino que deben tomar acciones inmediatas.

Asimismo, las empresas muy dependientes por facturación de la contratación pública deben igualmente tomar medidas, más si cabe a la luz del [Plan español para la contratación pública ecológica 2018-2025](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2019-1394).

Otro tanto ocurre con aquellas empresas que deseen acceder a **financiación**, que cada vez tomará más en cuenta criterios de sostenibilidad conforme a los Reglamentos UE 2019/2088 y 2020/852. Estos Reglamentos aluden al impacto ambiental de las inversiones, al cambio climático y particularmente a indicadores que lo miden como la huella de carbono.

## 3) Resiliencia al cambio normativo y competitividad

Los modelos de gestión modernos son más preventivos que reactivos. En esta línea, resulta aconsejable que las empresas se anticipan razonablemente a estas novedades normativas. La sostenibilidad y la resiliencia al cambio normativo son un vector de competitividad en un entorno complejo y cambiante. En Segara nos guían los principales estándares internacionales (ISO 14064, GHG Protocol, y toda la legislación aplicable antes mencionada), a fin de ofrecer rigor, calidad y tranquilidad ante estas novedades.

![Pantallazo de SegaraCO2](../../assets/images/blog/segara-CO2-emisiones.jpg)
_El sector farmacéutico es un 28% menor que el automovilístico, pero contamina un 13% más. Solamente con datos precisos y mediciones automatizadas podemos tomar decisiones informadas en la empresa. Fuente: [Carbon footprint of the global pharmaceutical industry and relative impact of its major players](https://www.researchgate.net/publication/329666480_Carbon_footprint_of_the_global_pharmaceutical_industry_and_relative_impact_of_its_major_players)._

## 4) Una oportunidad de explotar datos y conocer mejor sus propios procesos

La adaptación a esta legislación incipiente puede permitir a las empresas extraer de forma integrada y automatizada **datos** a los que, hasta ahora, no se les está extrayendo el máximo provecho. Por tanto, disponer de datos precisos también constituye una oportunidad para las propias empresas.

Conocerán mejor, entre otros ejemplos posibles, si hay un desfase entre curvas de uso y de consumo de energía en su organización, patrones ineficientes en rutas logísticas, **integraciones** con herramientas informáticas de contabilidad, vinculando todo ello a su desempeño en sostenibilidad.
